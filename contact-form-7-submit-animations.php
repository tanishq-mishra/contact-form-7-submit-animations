<?php

/**
 * @package ContactForm7SubmitAnimations
 */

/*
Plugin Name: Contact Form 7 Submit Animations
Plugin URI: https://www.greyhatch.com/
Description: Customizable Particle Effects animations for contact form 7.
Version: 1.0.0
Author: Grey Hatch
Author URI:  https://www.greyhatch.com/
License: GPLv2 or later
Text Domain: contact-form-7-submit-animations
*/

defined('ABSPATH') or die("don't sneak around");

if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
    require_once dirname(__FILE__) . '/vendor/autoload.php';
}

function activate_wpcf7_submit_animations()
{
    Inc\Base\Activate::activate();
}
function deactivate_wpcf7_submit_animations()
{
    Inc\Base\Deactivate::deactivate();
}

register_activation_hook(__FILE__, 'activate_wpcf7_submit_animations');
register_deactivation_hook(__FILE__, 'deactivate_wpcf7_submit_animations');

if (class_exists('Inc\\Init')) {
    Inc\Init::register_services();
}
