<?php

/**
 * @package ContactForm7SubmitAnimations
 */

namespace Inc\Base;

class Activate
{
    public static function activate()
    {
        // echo "Activated";
        flush_rewrite_rules();

        if (!get_option('fetchdata_plugin')) {
            update_option('fetchdata_plugin', array());
        }
    }
}
