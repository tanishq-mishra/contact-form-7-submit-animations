<?php

/**
 * @package ContactForm7SubmitAnimations
 */

namespace Inc\Base;


class BaseController
{
	public $plugin_path;

	public $plugin_url;

	public $plugin;

	public $dropdowns = array();

	public function __construct()
	{
		$this->plugin_path = plugin_dir_path(dirname(__FILE__, 2));
		$this->plugin_url = plugin_dir_url(dirname(__FILE__, 2));
		$this->plugin = plugin_basename(dirname(__FILE__, 3)) . '/contact-form-7-submit-animations.php';
		$this->dropdowns = array(
			'type' => array(
				'title' => 'Choose the Type',
				'callback_function' => 'dropdownField',

				'options' => array(
					'circle', 'rectangle', 'triangle'
				)
			),
			'style' => array(
				'title' => 'Choose the Style',
				'callback_function' => 'dropdownField',

				'options' => array(
					'fill', 'stroke'

				)
			),
			'direction' => array(
				'title' => 'Choose the Direction',
				'callback_function' => 'dropdownField',

				'options' => array(
					'left', 'right', 'top', 'bottom'

				)
			),
			'oscillationCoefficient' => array(
				'title' => 'Enter the Velocity of Particles',
				'callback_function' => 'inputField',
				'placeholder' => '10-100'
			),
			'duration' => array(
				'title' => 'Enter the Duration',
				'callback_function' => 'inputField',
				'placeholder' => 'Duration in ms'
			),
			'size' => array(
				'title' => 'Enter the Size',
				'callback_function' => 'inputField',
				'placeholder' => 'Between 1 - 20'
			),
			'color' => array(
				'title' => 'Choose a Color',
				'callback_function' => 'colorField'
			)
		);
	}
}
