<?php

/**
 * @package ContactForm7SubmitAnimations
 */

namespace Inc\Base;

class Deactivate
{
    public static function deactivate()
    {
        flush_rewrite_rules();
    }
}
