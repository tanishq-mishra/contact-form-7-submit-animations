<?php

/**
 * @package ContactForm7SubmitAnimations
 */

namespace Inc\Base;

use Inc\Base\BaseController;

class Enqueue extends BaseController
{

    function register()
    {
        add_action('wp_enqueue_scripts', array($this, 'enqueue'), 11);
    }
    function enqueue()
    {
        wp_enqueue_style('particles', $this->plugin_url . '/assets/particles.css');
        wp_enqueue_script('particlesjs', $this->plugin_url . '/assets/particles.js', array(), false, true);
        wp_enqueue_script('driver', $this->plugin_url . '/assets/driver.js', array('particlesjs'), false, true);
    }
}
