<?php

/**
 * @package ContactForm7SubmitAnimations
 */

namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

final class AdminCallbacks extends BaseController
{
    public function adminDashboard()
    {
        return require_once("$this->plugin_path/templates/admin.php");
    }

    public function adminSectionManager()
    {
        echo "Select your options";
    }
    public function inputSanitize($input)
    {
        return $input;
    }

    public function inputField($args)
    {
        $name = $args['label_for'];
        $option_name = $args['option_name'];
        $data = get_option($option_name);
        $value = isset($data[$name]) ? $data[$name] : null;
        $placeholder = $args['placeholder'];
        echo '<input 
            type="number" 
            name="' . $option_name . '[' . $name . ']"
            id="' . $name . '"
            value="' . $value . '"
            placeholder="' . $placeholder . '"
            >';
    }
    public function colorField($args)
    {
        $name = $args['label_for'];
        $option_name = $args['option_name'];
        $data = get_option($option_name);
        $selected = isset($data[$name]) ? $data[$name] : null;
        echo '<input 
            type="color" 
            name="' . $option_name . '[' . $name . ']"
            id="' . $name . '"
            value="' . $selected . '"
            >';
    }
    public function dropdownField($args)
    {
        $options = '';
        $name = $args['label_for'];
        $option_name = $args['option_name'];
        $data = get_option($option_name);
        $values = $args['options'];
        $selected = isset($data[$name]) ? $data[$name] : null;
        foreach ($values as $value) {
            $options = $options . '<option name="' . $value . '" value="' . $value . '"' . ($selected == $value ? 'selected' : null) . '>' . $value . '</option>';
        }

        echo '
        <select
        name="' . $option_name . '[' . $name . ']"
        id="' . $name . '">'
            . $options .
            '</select>';
    }
}
